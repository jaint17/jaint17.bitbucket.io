var indexSectionsWithContent =
{
  0: "_abcdefhlmrtu",
  1: "du",
  2: "defhl",
  3: "_bdfmrt",
  4: "l",
  5: "abclmrt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "groups",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Modules",
  5: "Pages"
};

