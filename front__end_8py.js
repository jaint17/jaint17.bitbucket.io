var front__end_8py =
[
    [ "extraction", "front__end_8py.html#gac299c30c77a3bc36f497471c5ccb101e", null ],
    [ "formattedArray", "front__end_8py.html#gac805eb4ed6ba8f168d14d39ed0309024", null ],
    [ "gen_CSV", "front__end_8py.html#gab1d695dbb85b636ce042ad481126fc25", null ],
    [ "getData", "front__end_8py.html#gab809d6e2e991845dd5a1936d4b8a55d4", null ],
    [ "interpretEntry", "front__end_8py.html#ga169ad18643c05033b1cea590267a5736", null ],
    [ "plot_figure", "front__end_8py.html#gaaeab68d05abd0abda33e15dc6b5899b4", null ],
    [ "baudrate", "front__end_8py.html#ga9104a2d1641449d3826329a31160bb2b", null ],
    [ "dataFormattedArray", "front__end_8py.html#ga71974dd9b56c9a1936c32dd8f9bc0eb5", null ],
    [ "dataList", "front__end_8py.html#gaddff09f9b364545f68669d77861bafb5", null ],
    [ "dataLoc", "front__end_8py.html#ga3c2a13c3f15f0434b78ca598166443d7", null ],
    [ "dataString", "front__end_8py.html#ga35a2f5365c16e36572ce055ab8bda9f0", null ],
    [ "n", "front__end_8py.html#ga0aee6cd5c168b373cf7d7678dfb8316c", null ],
    [ "port", "front__end_8py.html#ga9ed0c8a028d25324274e272ca07437f8", null ],
    [ "strippedString", "front__end_8py.html#ga13e1bd9ac3ffd04904335af9f93aefdf", null ],
    [ "timeout", "front__end_8py.html#ga2a0e281a48efba9fbb72a30a7bec876b", null ],
    [ "user_in", "front__end_8py.html#ga8a62b52b8795eb43c3eabb6a278c5a7d", null ]
];